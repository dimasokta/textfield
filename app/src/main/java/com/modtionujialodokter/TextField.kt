package com.modtionujialodokter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.core.view.setPadding
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class TextField: LinearLayout, View.OnFocusChangeListener {

    private lateinit var form: ConstraintLayout
    private lateinit var input: EditText
    private lateinit var label: TextView
    private lateinit var error: TextView
    private lateinit var progress: ProgressBar
    private lateinit var icon: ImageView

    private lateinit var formViewParams: LayoutParams
    private lateinit var inputTextParams: LayoutParams
    private lateinit var displayTextParams: LayoutParams
    private lateinit var errorTextParams: LayoutParams
    private lateinit var progressViewParams: LayoutParams
    private lateinit var iconViewParams: LayoutParams

    private lateinit var bottomUp: Animation
    private lateinit var bottomDown:Animation

    private val constraintSet = ConstraintSet()
    private var onFocus = false
    private lateinit var inputType: BorderInputType
    private var isEnable: Boolean = true

    private var onHintText = ""
    private var onProgressText = ""
    private var onSucceedText = ""
    private var onFailedText = ""
    private var onErrorText = ""

    private var onIconClickListener: OnIconClickListener? = null

    constructor(context: Context?) : super(context){
        createLayout(null)
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        if (!isInEditMode) {
            createLayout(attrs)
        }
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    private fun createLayout(attrs: AttributeSet?) {
        val context = context
        form = ConstraintLayout(context)
        input = EditText(context)
        label = TextView(context)
        error = TextView(context)
        progress = ProgressBar(context, null, android.R.attr.progressBarStyleSmall)
        icon = ImageView(context)
        input.onFocusChangeListener = this
        bottomUp = AnimationUtils.loadAnimation(context, R.anim.txt_bottom_up)
        bottomDown = AnimationUtils.loadAnimation(context, R.anim.txt_bottom_down)

        // Create Default Layout
        formViewParams = LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT)
        inputTextParams = LayoutParams(
                0,
                LayoutParams.WRAP_CONTENT)
        displayTextParams = LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT)
        errorTextParams = LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT)
        progressViewParams = LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT)
        iconViewParams = LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT)

        errorTextParams.topMargin = 20

        form.layoutParams = formViewParams
        input.layoutParams = inputTextParams
        label.layoutParams = displayTextParams
        error.layoutParams = errorTextParams
        progress.layoutParams = progressViewParams
        icon.layoutParams = iconViewParams
        orientation = VERTICAL

        createDefaultLayout()
        attrs?.let { createCustomLayout(it) }
        input.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (error.visibility == VISIBLE) clearError()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {}
        })

        icon.setOnClickListener {
            if (onIconClickListener!=null && inputType==BorderInputType.DATE && isEnable){
                onIconClickListener?.onDateClick()
            }
        }
    }

    private fun createDefaultLayout() {

        form.id = View.generateViewId()
        form.background = ContextCompat.getDrawable(context, R.drawable.bg_textfield_border)
        form.setPadding(16)

        label.id = View.generateViewId()
        label.gravity = Gravity.START
        label.setTextColor(ContextCompat.getColor(context, R.color.text_field_value))
        label.setPadding(10, 0, 5, 0)
        label.textSize = 12.0f
        label.typeface = Typeface.createFromAsset(context.assets, "Lato-Regular.ttf")
        label.visibility = GONE

        input.id = View.generateViewId()
        input.gravity = Gravity.START or Gravity.TOP
        input.setTextColor(ContextCompat.getColor(context, R.color.text_field_value))
        input.setPadding(5, 20, 20, 20)
        input.background = null
        input.textSize = 16.0f
        input.isSingleLine = false
        input.error = null
        input.typeface = Typeface.createFromAsset(context.assets, "Lato-Regular.ttf")

        error.gravity = Gravity.START
        error.setTextColor(ContextCompat.getColor(context, R.color.text_field_error))
        error.text = "error nih"
        error.textSize = 12.0f
        error.typeface = Typeface.createFromAsset(context.assets, "Lato-Regular.ttf")
        error.visibility = GONE

        progress.id = View.generateViewId()
        progress.setPadding(5, 5, 20, 5)
        progress.indeterminateTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.text_field_active))
        progress.visibility = GONE

        icon.id = View.generateViewId()
        icon.visibility = GONE

        addView(form)
        form.addView(label)
        form.addView(input)
        form.addView(progress)
        form.addView(icon)
        addView(error)
    }

    private fun createCustomLayout(attrs: AttributeSet) {
        val set = context.obtainStyledAttributes(attrs, R.styleable.BorderTextField, 0, 0)

        val enable = set.getBoolean(R.styleable.BorderTextField_enable, true)
        val inputEnable = set.getBoolean(R.styleable.BorderTextField_input_enable, true)
        val text = set.getString(R.styleable.BorderTextField_text)
        val hint = set.getString(R.styleable.BorderTextField_hint)
        val errorText = set.getString(R.styleable.BorderTextField_error_text)
        val progressText = set.getString(R.styleable.BorderTextField_progress_text)
        val succeedText = set.getString(R.styleable.BorderTextField_succeed_text)
        val failedText = set.getString(R.styleable.BorderTextField_failed_text)
        val iconDrawable = set.getDrawable(R.styleable.BorderTextField_drawable_icon)
        val progress = set.getBoolean(R.styleable.BorderTextField_show_progress, false)
        val showError = set.getBoolean(R.styleable.BorderTextField_show_error, false)
        val max = set.getInt(R.styleable.BorderTextField_max_length, -1)
        val type = BorderInputType.values()[set.getInt(R.styleable.BorderTextField_type,0)]

        setEnable(enable)
        setInputEnable(inputEnable)
        setText(text)
        setTextHint(hint)
        setTextError(errorText)
        setTextOnProgress(progressText)
        setTextOnSucceed(succeedText)
        setTextOnFailed(failedText)
        setIcon(iconDrawable)
        setMaxLength(max)
        setType(type)
        if(progress) setOnProgress()
        if(showError) setError()

        setPosition()
    }

    private fun setType(type: BorderInputType?) {
        if (type==null){
            inputType = BorderInputType.TEXT
            input.inputType = InputType.TYPE_CLASS_TEXT
        }else{
            inputType = type
            when(inputType){
                BorderInputType.DATE -> input.inputType = InputType.TYPE_CLASS_TEXT
                BorderInputType.PASSWORD -> input.transformationMethod = PasswordTransformationMethod.getInstance()
                BorderInputType.NUMBER -> input.inputType = InputType.TYPE_CLASS_NUMBER
                BorderInputType.TEXT -> input.inputType = InputType.TYPE_CLASS_TEXT
            }
        }

        Log.d("EDITTEXT", "type : "+inputType)
    }

    private fun setPosition() {
        //position
        constraintSet.clone(form)

        // post label
        constraintSet.connect(label.id, ConstraintSet.TOP, form.id, ConstraintSet.TOP, 16)
        constraintSet.connect(label.id, ConstraintSet.START, form.id, ConstraintSet.START, 16)

        // post input
        constraintSet.connect(input.id, ConstraintSet.TOP, label.id, ConstraintSet.BOTTOM, 0)
        constraintSet.connect(input.id, ConstraintSet.START, form.id, ConstraintSet.START, 20)
        if (icon.visibility == VISIBLE){
            constraintSet.connect(input.id, ConstraintSet.END, icon.id, ConstraintSet.START, 5)
        }else{
            constraintSet.connect(input.id, ConstraintSet.END, progress.id, ConstraintSet.START, 5)
        }

        // progress input
        constraintSet.connect(progress.id, ConstraintSet.TOP, input.id, ConstraintSet.TOP, 20)
        constraintSet.connect(progress.id, ConstraintSet.END, form.id, ConstraintSet.END, 20)

        // icon input
        constraintSet.connect(icon.id, ConstraintSet.TOP, input.id, ConstraintSet.TOP, 20)
        constraintSet.connect(icon.id, ConstraintSet.END, form.id, ConstraintSet.END, 20)

        constraintSet.applyTo(form)
    }

    private fun showHint() {
        if (label.visibility != VISIBLE) {
            label.visibility = VISIBLE
            label.startAnimation(bottomUp)
        }
    }

    private fun hideHint() {
        if (label.visibility != INVISIBLE) {
            label.visibility = INVISIBLE
            label.startAnimation(bottomDown)
            bottomDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(p0: Animation?) {}

                override fun onAnimationEnd(p0: Animation?) {
                    label.visibility = GONE
                }

                override fun onAnimationRepeat(p0: Animation?) {}
            })
        }
    }

    private fun onFocusBackground(onFocus: Boolean){
        if (onFocus){
            form.background?.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(context, android.R.color.holo_blue_dark), BlendModeCompat.SRC_ATOP)
            label.setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_dark))
        }else{
            form.background?.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(context, android.R.color.black), BlendModeCompat.SRC_ATOP)
            label.setTextColor(ContextCompat.getColor(context, android.R.color.black))
        }
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        onFocus = hasFocus
        label.isSelected = onFocus
        onFocusBackground(onFocus)
        if (!onFocus && input.text.toString().isEmpty()){
            hideHint()
        }else{
            showHint()
        }
    }

    fun setEnable(enable: Boolean?) {
        if (enable != null) {
            isEnable = enable
            input.isEnabled = isEnable
            if (isEnable) {
                form.background = ContextCompat.getDrawable(context, R.drawable.bg_textfield_border)
            } else {
                label.visibility = VISIBLE
                form.background = ContextCompat.getDrawable(context, R.drawable.bg_textfield_disable)
                input.setTextColor(ContextCompat.getColor(context, R.color.text_field_normal))
                label.setTextColor(ContextCompat.getColor(context, R.color.text_field_normal))
            }
        }
    }

    fun setInputEnable(inputEnable: Boolean?) {
        if (inputEnable != null) {
            input.isEnabled = inputEnable
        }
    }

    fun setIcon(iconDrawable: Drawable?) {
        if (iconDrawable!=null){
            icon.setImageDrawable(iconDrawable)
            icon.visibility = VISIBLE
        }
    }

    fun setError(){
        form.background?.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(context, R.color.text_field_error), BlendModeCompat.SRC_ATOP)
        label.setTextColor(ContextCompat.getColor(context, R.color.text_field_error))
        error.setTextColor(ContextCompat.getColor(context, R.color.text_field_error))
        error.text = onErrorText
        if (onErrorText.isNotEmpty()){
            error.visibility = VISIBLE
        }
    }

    fun clearError(){
        if (!onFocus) form.background?.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(context, R.color.text_field_normal), BlendModeCompat.SRC_ATOP)
        if (!onFocus) label.setTextColor(ContextCompat.getColor(context, R.color.text_field_normal))
        error.visibility = GONE
        icon.visibility = GONE
        progress.visibility = GONE
    }

    fun setOnProgress(){
        error.setTextColor(ContextCompat.getColor(context, R.color.text_field_normal))
        error.text = onProgressText
        error.visibility = VISIBLE
        progress.visibility = VISIBLE
        icon.visibility = GONE
    }

    fun setOnSucceed(){
        error.setTextColor(ContextCompat.getColor(context, R.color.text_field_normal))
        error.text = onSucceedText
        error.visibility = VISIBLE
        progress.visibility = INVISIBLE

        icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_succeed))
        icon.visibility = VISIBLE
    }

    fun setOnFailed(){
        error.setTextColor(ContextCompat.getColor(context, R.color.text_field_normal))
        error.text = onFailedText
        error.visibility = VISIBLE
        progress.visibility = INVISIBLE

        icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_failed))
        icon.visibility = VISIBLE
    }

    fun getText(): String = input.text?.toString().orEmpty().trim()

    fun setText(text: String?) {
        if (!text.isNullOrEmpty()) {
            label.visibility = VISIBLE
            input.setText(text)
        }
    }

    fun setTextHint(hintText: String?) {
        if (hintText != null) {
            onHintText = hintText
            input.hint = hintText
            label.text = hintText
        }
    }

    fun setTextError(message: String?) {
        if (message != null) {
            onErrorText = message
            error.text = onErrorText
        }
    }

    fun setTextOnProgress(message: String?) {
        if (message != null) {
            onProgressText = message
        }
    }

    fun setTextOnSucceed(message: String?) {
        if (message != null) {
            onSucceedText = message
        }
    }

    fun setTextOnFailed(message: String?) {
        if (message != null) {
            onFailedText = message
        }
    }

    fun setMaxLength(max: Int?){
        if (max != null && max>0){
            input.filters = arrayOf(InputFilter.LengthFilter(max))
        }
    }

//    fun isEmptyString(input: String?): Boolean? {
//        return input == null || input.trim { it <= ' ' }.isEmpty()
//    }

    fun validateNotEmpty() : Boolean{
        if (getText().isEmpty()){
            onErrorText = "$onHintText wajib Anda isi"
            setError()
            return false
        }
        return true
    }

    fun validateLength(min: Int, message: String) : Boolean{
        if (getText().length < min){
            onErrorText = message
            setError()
            return false
        }
        return true
    }

    fun validateKtp(message: String) : Boolean{
        if (!Pattern.compile(
                "^(?:1(?!23456)|2(?!34567)|3(?!45678)|4(?!56789)|5(?!43210)|9(?!87654)|8(?!76543)|7(?!65432)|6(?!54321)|(?<!0))\\d{5}([1-2][0-9]|0[1-9]|3[0-1]|[5-6][0-9]|4[1-9]|7[0-1])(0[0-9]|1[0-2])\\d{2}\\d{4}$"
                , Pattern.CASE_INSENSITIVE).matcher(getText()).find()){
            onErrorText = message
            setError()
            return false
        }
        return true
    }

    fun validateCustomRegex(pattern: String, message: String) : Boolean{
        if (!Pattern.compile(pattern, Pattern.CASE_INSENSITIVE).matcher(getText()).find()){
            onErrorText = message
            setError()
            return false
        }
        return true
    }

    fun validateWords(min: Int, message: String) : Boolean{
        val trim = getText().trim { it <= ' ' }
        val count = if (trim.isEmpty()) 0 else trim.split("\\s+".toRegex()).toTypedArray().size
        if (count < min){
            onErrorText = message
            setError()
            return false
        }
        return true
    }

    fun validateAge(minAge: Int, maxAge: Int, pattern: String, message: String) : Boolean{
//        "dd/MM/yyyy"
        val formatter = SimpleDateFormat(pattern, Locale.getDefault())
        val dateOfBirthday = try {
            formatter.parse(getText())
        } catch (e: ParseException) {
            Date()
        }
        val ageUser = Calendar.getInstance().apply { time = Date() }.get(Calendar.YEAR) -
                Calendar.getInstance().apply { time = dateOfBirthday }.get(Calendar.YEAR)
        if (ageUser < minAge || ageUser > maxAge){
            onErrorText = message
            setError()
            return false
        }
        return true
    }

    fun input() : EditText = input

    fun label() : TextView = label

    fun error() : TextView = error

    fun icon() : ImageView = icon

    fun progress() : ProgressBar = progress

    fun setOnIconClickListener(listener: OnIconClickListener){
        this.onIconClickListener = listener
    }

}