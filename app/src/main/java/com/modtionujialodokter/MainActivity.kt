package com.modtionujialodokter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.modtionujialodokter.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        binding.label1.apply {
//            setTextHint("hint by program")
//            setTextError("ada errornya lagi")
//        }

        binding.check.setOnClickListener {
            check()
        }

        binding.edtIcon.setOnIconClickListener(object : OnIconClickListener {
            override fun onDateClick() {
                Toast.makeText(this@MainActivity, "date clicked", Toast.LENGTH_SHORT).show()
            }
        })

        binding.edtIconDisable.setOnIconClickListener(object : OnIconClickListener {
            override fun onDateClick() {
                Toast.makeText(this@MainActivity, "date clicked", Toast.LENGTH_SHORT).show()
            }
        })

        binding.edtNormal.input().addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}
        })
    }

    private fun check() {
        var isValid = true
        if (!binding.edtNormal.validateNotEmpty()) isValid = false

        Toast.makeText(this, isValid.toString(), Toast.LENGTH_SHORT).show()
    }
}