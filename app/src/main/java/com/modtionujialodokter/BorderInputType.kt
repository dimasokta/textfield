package com.modtionujialodokter

enum class BorderInputType {
    TEXT, NUMBER, DATE, PASSWORD
}